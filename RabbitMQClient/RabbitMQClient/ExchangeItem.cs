﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQClient
{
    public class ExchangeItem:IDisposable
    {
        string exchangeName;
        string exchangeType;
        IModel channel;
        IConnection connection;
        ConnectionFactory connectionFactory;
        object locker = new object();
        /// <summary>
        /// 创建一个Exchange对象,若对象在MQ中不存在则自动创建。
        /// </summary>
        /// <param name="_ExchangeName">Exchange名称</param>
        /// <param name="_type">Exchange类型，Direct发布给指定Route绑定队列，FANOUT发送给全部绑定队列，TOPIC发送给筛选队列</param>
        /// <param name="_connection"></param>
        public ExchangeItem(string _ExchangeName, ExchangeType _type,IConnection _connection,ConnectionFactory _connectionFactory)
        {
            exchangeName = _ExchangeName;
            exchangeType = _type.ToString().ToLower();
            connection = _connection;
            connectionFactory = _connectionFactory;
            if (!connection.IsOpen)
            {
                connection = connectionFactory.CreateConnection();
            }
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchangeName, _type.ToString().ToLower(), true,false);//创建EXCHANGE
        }
        /// <summary>
        /// 绑定一个Queue(自动创建Queue)
        /// </summary>
        /// <param name="_QueueName">Queue名称</param>
        /// <param name="_Routing">Route名称</param>
        /// <returns></returns>
        public bool BindQueue(string _QueueName,string _Routing="")
        {
            try
            {
                CheckChannel();
                channel.QueueDeclare(_QueueName, true, false, false, null);
                channel.QueueBind(_QueueName, exchangeName, _Routing, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 解绑一个Queue
        /// </summary>
        /// <param name="_QueueName">Queue名称</param>
        /// <returns></returns>
        public bool UnBindQueue(string _QueueName)
        {
            try
            {
                CheckChannel();
                channel.QueueUnbind(_QueueName, exchangeName, "", null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 对此交换机发布一条消息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="Routing"></param>
        /// <returns></returns>
        public bool Send(object msg,string Routing="")
        {
            CheckChannel();
            string _message = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(_message);
            channel.BasicPublish(exchangeName, Routing, null, body);
            return true;
        }
        private void CheckChannel()
        {
            if (!channel.IsOpen)
            {
                channel = connection.CreateModel();
            }
        }

        public void Dispose()
        {
            channel.Close();
        }
    }
    public enum ExchangeType
    {
        DIRECT,
        FANOUT,
        TOPIC,
        HEAERS
    }
}
