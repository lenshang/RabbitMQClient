﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;
using Newtonsoft.Json;
namespace RabbitMQClient
{
    public class RpcClient
    {
        IModel channel;
        IConnection connection;
        ConnectionFactory connectionFactory;
        string RpcQueue = "";
        string ReplayQueue = "";
        string RpcExchange = "";
        /// <summary>
        /// 创建一个RPC回调客户端
        /// </summary>
        /// <param name="_connection"></param>
        /// <param name="_connectionFactory"></param>
        public RpcClient(string _RpcExchange,string _RpcQueue,IConnection _connection, ConnectionFactory _connectionFactory)
        {
            RpcQueue = _RpcQueue;
            connection = _connection;
            connectionFactory = _connectionFactory;
            RpcExchange = _RpcExchange;
            if (!connection.IsOpen)
            {
                connection = connectionFactory.CreateConnection();
            }
            using(IModel channel = connection.CreateModel())
            {
                if (!string.IsNullOrEmpty(RpcQueue))
                {
                    channel.QueueDeclare(RpcQueue, false, false, false, null);//声明一个RPC队列
                }
                if (!string.IsNullOrEmpty(RpcExchange))
                {
                    channel.ExchangeDeclare(RpcExchange, ExchangeType.FANOUT.ToString().ToLower(), true, false, null);//声明Exchange队列
                }
                ReplayQueue = channel.QueueDeclare().QueueName;//声明一个匿名回馈队列QUEUE
            }
        }
        /// <summary>
        /// 发送一条消息并接收回馈
        /// </summary>
        /// <param name="msg">消息对象</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public string Send(object msg, TimeSpan? timeOut = null)
        {
            Task<string> tresult = SendAsync(msg, timeOut);
            tresult.Wait();
            return tresult.Result;
        }
        /// <summary>
        /// 发送一条消息并接收回馈
        /// </summary>
        /// <typeparam name="T">返回消息对象类型</typeparam>
        /// <param name="msg">消息对象</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public T Send<T>(object msg, TimeSpan? timeOut = null)
        {
            Task<T> tresult = SendAsync<T>(msg, timeOut);
            tresult.Wait();
            return tresult.Result;
        }
        /// <summary>
        /// 异步发送一条消息并接收回馈
        /// </summary>
        /// <param name="msg">消息对象</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public async Task<string> SendAsync(object msg, TimeSpan? timeOut = null)
        {
            CheckChannel();
            var corrId = Guid.NewGuid().ToString();
            var prop = channel.CreateBasicProperties();
            prop.ReplyTo = ReplayQueue;
            prop.CorrelationId = corrId;

            var msgstr = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(msgstr);
            channel.BasicPublish("", RpcQueue, prop, body);


            var consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(ReplayQueue, true, consumer);
            string result = null;
            bool IsReply = false;
            consumer.Received += (ch, ea) =>
            {
                if (ea.BasicProperties.CorrelationId == corrId)
                {
                    result = Encoding.UTF8.GetString(ea.Body);
                    IsReply = true;
                    //Console.WriteLine(result);
                }
            };
            return await Task.Run(()=> {
                DateTime stime = DateTime.Now;
                while ((!IsReply)&& GetTimeOut(stime,timeOut))
                {
                    Thread.Sleep(100);
                }
                return result;
            });

        }
        /// <summary>
        /// 异步发送一条消息并接收回馈
        /// </summary>
        /// <typeparam name="T">返回消息对象类型</typeparam>
        /// <param name="msg">消息对象</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public async Task<T> SendAsync<T>(object msg, TimeSpan? timeOut =null)
        {
            CheckChannel();
            var corrId = Guid.NewGuid().ToString();
            var prop = channel.CreateBasicProperties();
            prop.ReplyTo = ReplayQueue;
            prop.CorrelationId = corrId;

            var msgstr = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(msgstr);
            channel.BasicPublish("", RpcQueue, prop, body);

            var consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(ReplayQueue, true, consumer);
            T result = default(T);
            bool IsReply = false;
            consumer.Received += (ch, ea) =>
            {
                if (ea.BasicProperties.CorrelationId == corrId)
                {
                    var _result = Encoding.UTF8.GetString(ea.Body);
                    result = JsonConvert.DeserializeObject<T>(_result);
                    IsReply = true;
                    //Console.WriteLine(result);
                }
            };
            return await Task.Run(() => {
                DateTime stime = DateTime.Now;
                while ((!IsReply) && GetTimeOut(stime, timeOut))
                {
                    Thread.Sleep(100);
                }
                return result;
            });
        }

        /// <summary>
        /// 发布一条消息并接收反馈(Exchange群发模式)
        /// </summary>
        /// <param name="msg">消息对象</param>
        /// <param name="replyCount">接收反馈数量(超过这个数值时会停止接收反馈)</param>
        /// <returns></returns>
        public string[] SendExchange(object msg, int replyCount=1, TimeSpan? timeOut = null)
        {
            Task<string[]> tresult = SendExchangeAsync(msg, replyCount, timeOut);
            tresult.Wait();
            return tresult.Result;
        }
        /// <summary>
        /// 发送一条消息并接收回馈(Exchange群发模式)
        /// </summary>
        /// <param name="msg">消息对象</param>
        /// <param name="replyCount">接收反馈数量(超过这个数值时会停止接收反馈)</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public T[] SendExchange<T>(object msg, int replyCount=1, TimeSpan? timeOut = null)
        {
            Task<T[]> tresult = SendExchangeAsync<T>(msg, replyCount, timeOut);
            tresult.Wait();
            return tresult.Result;
        }
        /// <summary>
        /// 异步发送一条消息并接收回馈(Exchange群发模式)
        /// </summary>
        /// <param name="msg">消息对象</param>
        /// <param name="replyCount">接收反馈数量(超过这个数值时会停止接收反馈)</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public async Task<string[]> SendExchangeAsync(object msg,int replyCount=1, TimeSpan? timeOut = null)
        {
            CheckChannel();
            var corrId = Guid.NewGuid().ToString();
            var prop = channel.CreateBasicProperties();
            prop.ReplyTo = ReplayQueue;
            prop.CorrelationId = corrId;

            var msgstr = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(msgstr);
            channel.BasicPublish(RpcExchange, "", prop, body);

            var consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(ReplayQueue, true, consumer);
            List<string> result = null;
            int _reply = 0;
            consumer.Received += (ch, ea) =>
            {
                if (ea.BasicProperties.CorrelationId == corrId)
                {
                    result.Add(Encoding.UTF8.GetString(ea.Body));
                    _reply++;
                    //Console.WriteLine(result);
                }
            };
            return await Task.Run(() => {
                DateTime stime = DateTime.Now;
                while (!(_reply>=replyCount)&& GetTimeOut(stime,timeOut))
                {
                    Thread.Sleep(100);
                }
                return result.ToArray();
            });
        }
        /// <summary>
        /// 异步发送一条消息并接收回馈(Exchange群发模式)
        /// </summary>
        /// <typeparam name="T">返回消息对象类型</typeparam>
        /// <param name="msg">消息对象</param>
        /// <param name="replyCount">接收反馈数量(超过这个数值时会停止接收反馈)</param>
        /// <param name="timeOut">超时时间</param>
        /// <returns></returns>
        public async Task<T[]> SendExchangeAsync<T>(object msg, int replyCount=1, TimeSpan? timeOut = null)
        {
            CheckChannel();
            var corrId = Guid.NewGuid().ToString();
            var prop = channel.CreateBasicProperties();
            prop.ReplyTo = ReplayQueue;
            prop.CorrelationId = corrId;

            var msgstr = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(msgstr);
            channel.BasicPublish(RpcExchange, "", prop, body);

            var consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(ReplayQueue, true, consumer);
            List<T> result = new List<T>();
            int _reply = 0;
            consumer.Received += (ch, ea) =>
            {
                if (ea.BasicProperties.CorrelationId == corrId)
                {
                    var _result = Encoding.UTF8.GetString(ea.Body);
                    result.Add(JsonConvert.DeserializeObject<T>(_result));
                    _reply++;
                    //Console.WriteLine(result);
                }
            };
            return await Task.Run(() => {
                DateTime stime = DateTime.Now;
                while (!(_reply >= replyCount) && GetTimeOut(stime, timeOut))
                {
                    Thread.Sleep(100);
                }
                return result.ToArray();
            });
        }

        /// <summary>
        /// 在监听反馈超时时发生
        /// </summary>
        public event Action OnListenReceiveTimeOut;

        private void CheckChannel()
        {
            if (channel == null)
            {
                channel = connection.CreateModel();
            }
            if (!channel.IsOpen)
            {
                channel = connection.CreateModel();
            }
        }
        private bool GetTimeOut(DateTime stime,TimeSpan? timeOut)
        {
            if (timeOut == null)//当没有超时时间时则无限久
            {
                return true;
            }
            DateTime etime = stime.Add(timeOut.Value);
            if (DateTime.Now>=etime)//超时时发生
            {
                OnListenReceiveTimeOut?.Invoke();
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
