﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQClient
{
    class Config
    {
        private static object locker=new object();
        private static IConnection connection;
        public static string Host = "";
        public static string User = "";
        public static string Password = "";
        /// <summary>
        /// 获得连接工厂对象
        /// </summary>
        /// <param name="HostName">服务器地址</param>
        /// <param name="UserName">用户名</param>
        /// <param name="Password">密码</param>
        /// <returns></returns>
        public static ConnectionFactory GetConnectionInfo(string _HostName= null, string _UserName= null, string _Password= null)
        {
            if (_HostName != null) Host = _HostName;
            if (_UserName != null) User = _UserName;
            if (_Password != null) Password = _Password;

            var factory = new ConnectionFactory();
            factory.HostName = Host;
            factory.UserName = User;
            factory.Password = Password;
            return factory;
        }

        public static IConnection GetConnection()
        {
            lock (locker)
            {
                if (connection == null || connection.IsOpen == false)
                {
                    connection = GetConnectionInfo().CreateConnection();
                }
            }
            return connection;
        }
        /// <summary>
        /// 是否储存在硬盘上
        /// </summary>
        public static bool IsDurable { get; set; } = false;
    }
}
