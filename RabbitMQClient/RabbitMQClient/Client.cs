﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using System.Threading;
using RabbitMQ.Client.MessagePatterns;

namespace RabbitMQClient
{
    public class Client
    {
        ConnectionFactory connectionFactory;
        IConnection connection;
        Dictionary<string, ExchangeItem> _Exchanges;//Exchange缓存
        Dictionary<string, IModel> Channels;//频道缓存
        object locker = new object();
        public Client(string Host="localhost",string User="guest",string Password="")
        {
            connectionFactory = Config.GetConnectionInfo(Host,User,Password);
            _Exchanges = new Dictionary<string, ExchangeItem>();
            Channels = new Dictionary<string, IModel>();
            connection = connectionFactory.CreateConnection();
            connection.AutoClose = true;
        }
        /// <summary>
        /// 获得一个Exchange，若没有则被创建
        /// </summary>
        /// <param name="exchangeName">Exchange名称</param>
        /// <param name="type">Exchange类型，若已存在Exchange，此参数无效</param>
        /// <returns></returns>
        public ExchangeItem GetExchange(string exchangeName,ExchangeType type=ExchangeType.DIRECT)
        {
            if (_Exchanges.ContainsKey(exchangeName))
            {
                return _Exchanges[exchangeName];
            }
            ExchangeItem _ex = new ExchangeItem(exchangeName, type, connection, connectionFactory);
            _Exchanges.Add(exchangeName, _ex);
            return _ex;
        }
        /// <summary>
        /// 直接发送一条数据到Exchange(不自动创建)
        /// </summary>
        /// <param name="item">数据</param>
        /// <param name="channelName">交换机名称</param>
        /// <param name="routing">路由名称</param>
        /// <returns></returns>
        public bool SendToExchange(object item, string exchangeName,string routing="")
        {
            try
            {
                lock (locker)
                {
                    if (connection.IsOpen == false)
                    {
                        connection = connectionFactory.CreateConnection();
                    }
                }
                using (var channel = connection.CreateModel())
                {
                    string _message = JsonConvert.SerializeObject(item);
                    var body = Encoding.UTF8.GetBytes(_message);
                    channel.BasicPublish(exchangeName, routing, null, body);
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// 直接发送一条数据到Queue(不自动创建)(不推荐使用)
        /// </summary>
        /// <param name="item">发送的数据</param>
        /// <param name="queueName">队列名称</param>
        /// <returns></returns>
        public bool SendToQueue(object item,string queueName)
        {
            try
            {
                lock (locker)
                {
                    if (connection.IsOpen == false)
                    {
                        connection = connectionFactory.CreateConnection();
                    }
                }
                using (var channel = connection.CreateModel())
                {
                    string _message = JsonConvert.SerializeObject(item);
                    var body = Encoding.UTF8.GetBytes(_message);
                    channel.BasicPublish("", queueName,false,null,body);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 监听一个queue(获得消息成功后反馈，消息将被删除）
        /// </summary>
        /// <param name="queue">队列名称</param>
        public async Task QueueListener<T>(string queue,Action<T> DoMsg)
        {
            if (connection.IsOpen == false)
            {
                connection = connectionFactory.CreateConnection();
            }
            var channel = GetQueueChannel(queue);
            if (channel == null)
            {
                channel = connection.CreateModel();
                Channels.Add("q_" + queue, channel);//将Channel加入缓存
            }
            else if (channel.IsClosed)
            {
                channel = connection.CreateModel();
            }
            await Task.Run(()=> {
                channel.QueueDeclare(queue, true, false, false, null);//声明队列
                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                channel.BasicQos(0, 1, false);
                consumer.Received += (ch, ea) =>
                {
                    RecieveMsgEvent(DoMsg, ea, channel);
                };
                channel.BasicConsume(queue, false, consumer);
                while (channel.IsOpen)
                {
                    Thread.Sleep(100);
                }
            });
        }
        /// <summary>
        /// 监听一个Exchange(创建临时队列监听消息,建议针对此类EXCHANGE创建独立对象)(如果Exchange不存在 则会被创建)
        /// </summary>
        /// <param name="exchange">ExchangeName</param>
        public async Task ExchangeListener<T>(string exchange, Action<T> DoMsg)
        {
            if (connection.IsOpen == false)
            {
                connection = connectionFactory.CreateConnection();
            }
            var channel = GetExchangeChannel(exchange);
            if (channel == null)
            {
                channel = connection.CreateModel();
                Channels.Add("e_" + exchange, channel);//将Channel加入缓存
            }
            else if (channel.IsClosed)
            {
                channel = connection.CreateModel();
            }
            channel.ExchangeDeclare(exchange, ExchangeType.FANOUT.ToString().ToLower(),true,false ,null);
            channel.BasicQos(0, 1, false);
            string _tempQueue = channel.QueueDeclare().QueueName;//获得一个随机的Queue名称
            channel.QueueBind(_tempQueue, exchange, "");
            channel.BasicQos(0, 1, false);
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                RecieveMsgEvent(DoMsg, ea, channel);
            };
            channel.BasicConsume(_tempQueue, false, consumer);
            await Task.Run(()=> {
                while (channel.IsOpen)
                {
                    Thread.Sleep(100);
                }
            });
        }

        #region RPC服务
        /// <summary>
        /// 获得一个RPC客户端对象
        /// </summary>
        /// <param name="queue">Queue名称</param>
        /// <returns></returns>
        public RpcClient GetRpcClient(string exchange,string queue)
        {
            RpcClient rc = new RpcClient(exchange,queue, connection, connectionFactory);
            return rc;
        }
        /// <summary>
        /// 监听一个RPC队列，并指定回馈消息
        /// </summary>
        /// <typeparam name="T">消息对象类型</typeparam>
        /// <param name="queue">Queue名称</param>
        /// <param name="DoMsg">处理消息对象</param>
        public async Task RpcQueueListener<T>(string queue, Func<T, object> DoMsg)
        {
            if (connection.IsOpen == false)
            {
                connection = connectionFactory.CreateConnection();
            }
            var channel = GetQueueChannel(queue);
            if (channel == null)
            {
                channel = connection.CreateModel();
                Channels.Add("q_" + queue, channel);//将Channel加入缓存
            }
            else if(channel.IsClosed)
            {
                channel = connection.CreateModel();
            }
            channel.QueueDeclare(queue, false, false, false, null);
            channel.BasicQos(0, 1, false);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                var body = ea.Body;
                var str = Encoding.UTF8.GetString(body);
                T item = JsonConvert.DeserializeObject<T>(str);
                var replyMsg = DoMsg(item);

                string replystr = JsonConvert.SerializeObject(replyMsg);
                var props = ea.BasicProperties;
                var replyProps = channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;

                channel.BasicPublish("", props.ReplyTo, replyProps, Encoding.UTF8.GetBytes(replystr));
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue, false, consumer);
            await Task.Run(()=> {
                while (channel.IsOpen)
                {
                    Thread.Sleep(100);
                }
            });
        }
        /// <summary>
        /// 监听一个Exchange(FANOUT模式),并指定回馈消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Exchange">Exchange名称</param>
        /// <param name="DoMsg">处理消息方法</param>
        /// <returns></returns>
        public async Task RpcExchangeListener<T>(string exchange, Func<T, object> DoMsg)
        {
            if (connection.IsOpen == false)
            {
                connection = connectionFactory.CreateConnection();
            }
            var channel = GetExchangeChannel(exchange);
            if (channel == null)
            {
                channel = connection.CreateModel();
                Channels.Add("e_" + exchange, channel);//将Channel加入缓存
            }
            else if (channel.IsClosed)
            {
                channel = connection.CreateModel();
            }
            channel.ExchangeDeclare(exchange, ExchangeType.FANOUT.ToString().ToLower(), true, false, null);
            string _queue = channel.QueueDeclare().QueueName;
            channel.QueueBind(_queue, exchange, "", null);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                var body = ea.Body;
                var str = Encoding.UTF8.GetString(body);
                T item = JsonConvert.DeserializeObject<T>(str);
                var replyMsg = DoMsg(item);

                string replystr = JsonConvert.SerializeObject(replyMsg);
                var props = ea.BasicProperties;
                var replyProps = channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;

                channel.BasicPublish("", props.ReplyTo, replyProps, Encoding.UTF8.GetBytes(replystr));
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(_queue, false, consumer);
            await Task.Run(() => {
                while (channel.IsOpen)
                {
                    Thread.Sleep(100);
                }
            });
        }
        #endregion

        /// <summary>
        /// 获得已创建监听Queue的Channel
        /// </summary>
        /// <param name="queue"></param>
        /// <returns></returns>
        public IModel GetQueueChannel(string queue)
        {
            string name = "q_" + queue;
            if (Channels.Keys.Contains(name))
            {
                return Channels[name];
            }
            return null;
        }
        /// <summary>
        /// 获得已创建监听Exchange的Channel
        /// </summary>
        /// <param name="exchange"></param>
        /// <returns></returns>
        public IModel GetExchangeChannel(string exchange)
        {
            string name = "e_" + exchange;
            if (Channels.Keys.Contains(name))
            {
                return Channels[name];
            }
            return null;
        }

        private static void RecieveMsgEvent<T>(Action<T> DoMsg, BasicDeliverEventArgs ea, IModel _channel)
        {
            var body = ea.Body;
            var str = Encoding.UTF8.GetString(body);
            try
            {
                T data = JsonConvert.DeserializeObject<T>(str);
                DoMsg(data);
                _channel.BasicAck(ea.DeliveryTag, false);
            }
            catch//如果对象序列化失败
            {
                _channel.BasicAck(ea.DeliveryTag, false);//从队列中删除(如果要保留可去掉）
            }
        }
    }
}
