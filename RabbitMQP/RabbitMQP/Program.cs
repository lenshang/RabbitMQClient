﻿using RabbitMQ.Client;
using RabbitMQClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQP
{
    class Program
    {
        static RabbitMQClient.Client rclient;
        static void Main(string[] args)
        {
            rclient = new RabbitMQClient.Client("localhost","root","123456");
            Console.WriteLine("说明:Direct,指定Exchange并且绑定Queue发送消息 接收者从指定Queue接收消息\nFANOUT，所有接收者创建临时队列全部从指定Exchange获得消息");
            Console.WriteLine("1=Send Data With DirectExchange,\n2=Send Data With FANOUT Exchange,\n3=Receive Data From a Queue,\n4=Recieve Data From Exchange(FANOUT MODE)");
            Console.WriteLine("5=使用RPC服务发送一条信息\n6=使用RPC服务接收并反馈一条消息");
            Console.WriteLine("7=使用RPC服务发送一条Exchange群发模式信息\n8=使用RPC服务监听Exchange接收并反馈一条消息");
            string usertape = Console.ReadLine();
            if (usertape == "1")
            {
                Console.WriteLine("Please Input ExchangeName:");
                string exname = Console.ReadLine();
                while (true)
                {
                    Send(exname);
                }
            }
            else if (usertape == "2")
            {
                Console.WriteLine("Please Input ExchangeName:");
                string exname = Console.ReadLine();
                Send(exname, true);
            }
            else if (usertape == "3")
            {
                Console.WriteLine("Please Input QueueName:");
                string exname = Console.ReadLine();
                rclient.QueueListener<string>(exname, i => {
                    Console.WriteLine(i);
                }).ContinueWith(i=> {
                    Console.WriteLine("Listener 意外停止");
                });
            }
            else if(usertape=="4")
            {
                Console.WriteLine("Please Input ExchangeName:");
                string exname = Console.ReadLine();
                rclient.ExchangeListener<string>(exname, i=> {
                    Console.WriteLine(i);
                }).ContinueWith(i => {
                    Console.WriteLine("Listener 意外停止");
                });
            }
            else if (usertape == "5")
            {
                Console.WriteLine("Please Input QueueName:");
                string queuename = Console.ReadLine();
                SendRPC(queuename);
            }
            else if (usertape == "6")
            {
                Console.WriteLine("Please Input QueueName:");
                string queuename = Console.ReadLine();
                ReceiveRPC(queuename);
            }
            else if (usertape == "7")
            {
                Console.WriteLine("Please Input ExchangeName:");
                string exname = Console.ReadLine();
                SendRPCExchange(exname);
            }
            else if (usertape == "8")
            {
                Console.WriteLine("Please Input ExchangeName:");
                string exname = Console.ReadLine();
                ReceiveRPCExchange(exname);
            }
            Console.ReadLine();
            
        }
        static void Send(string exchangeName,bool isFanout=false,bool IsBinded=false)
        {
            ExchangeItem exitem;
            if (isFanout)
            {
                exitem = rclient.GetExchange(exchangeName,RabbitMQClient.ExchangeType.FANOUT);
            }
            else
            {
                exitem = rclient.GetExchange(exchangeName, RabbitMQClient.ExchangeType.DIRECT);
                if (!IsBinded)
                {
                    Console.WriteLine("请输入要绑定的Queue");
                    string queuename = Console.ReadLine();
                    exitem.BindQueue(queuename);
                    Console.WriteLine("还要继续绑定其他Queue吗？Y=是,N=否");
                    string userDecided = Console.ReadLine().ToLower();
                    if (userDecided == "y")
                    {
                        Send(exchangeName, isFanout, false);
                    }
                }
            }
            Console.WriteLine("Please Input Message:");
            string msg = Console.ReadLine();
            exitem.Send(msg);
            Console.WriteLine("Send Success!");
            Send(exchangeName, isFanout, true);
            //Console.ReadLine();
        }
        static void Receive()
        {

        }
        static void SendRPC(string queuename)
        {
            while (true)
            {
                Console.WriteLine("Please Enter the Message");
                string msg = Console.ReadLine();

                var rc = rclient.GetRpcClient("",queuename);
                var sendmsg = new Message()
                {
                    Date = DateTime.Now,
                    Name = "Sender",
                    msg = msg
                };
                rc.OnListenReceiveTimeOut += () => {
                    Console.WriteLine("接收超时！");
                };
                var replymsg = rc.Send<Message>(sendmsg,new TimeSpan(0,0,5));
                if (replymsg != null)
                {
                    Console.WriteLine($"{replymsg.Date}-{replymsg.Name}:{replymsg.msg}");
                }
            }
            //rclient.SendToQueue("test","RPCQueue");
        }
        static void ReceiveRPC(string queue)
        {
            //var rc=rclient.GetRpcClient("RPCQueue");
            //rclient.QueueListener<string>("RPCQueue", i => { Console.WriteLine(i); });
            rclient.RpcQueueListener<Message>(queue, i => {
                Console.WriteLine($"{i.Date}-{i.Name}:{i.msg}");
                var sendmsg = new Message()
                {
                    Date = DateTime.Now,
                    Name = "Replyer",
                    msg = $"Get Message({i.msg}) Success"
                };
                return sendmsg;
            }).ContinueWith(i=> { Console.WriteLine("Listener意外停止"); });
        }
        static void SendRPCExchange(string exchange)
        {
            while (true)
            {
                Console.WriteLine("Please Enter the Message");
                string msg = Console.ReadLine();

                var rc = rclient.GetRpcClient(exchange, "");
                var sendmsg = new Message()
                {
                    Date = DateTime.Now,
                    Name = "Sender",
                    msg = msg
                };
                var replymsg = rc.SendExchange<Message>(sendmsg,2,new TimeSpan(0,0,5));
                foreach(var item in replymsg)
                {
                    Console.WriteLine($"{item.Date}-{item.Name}:{item.msg}");
                }
            }
        }
        static void ReceiveRPCExchange(string exchange)
        {
            string name = Guid.NewGuid().ToString();
            rclient.RpcExchangeListener<Message>(exchange, i => {
                Console.WriteLine($"{i.Date}-{i.Name}:{i.msg}");
                var sendmsg = new Message()
                {
                    Date = DateTime.Now,
                    Name = "Replyer"+ name,
                    msg = $"Get Message({i.msg}) Success"
                };
                return sendmsg;
            }).ContinueWith(i => { Console.WriteLine("Listener意外停止"); });
        }
    }
    class Message
    {
        public string Name { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public string msg { get; set; }
    }
}
